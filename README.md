# TailwindUSS

Unity Style sheets that are a subset of https://tailwindcss.com/ for use with UI Toolkit (https://docs.unity3d.com/2021.2/Documentation/Manual/UIElements.html)

(c) 2021 Drop Fake Inc.
